package fr.pe.client.stream.kafka.service;

import fr.pe.client.stream.kafka.domain.Message;

/**
 *
 */
public interface ProcessMessageService {

    /**
     * traiter et notifier :
     * - telécharger le fichier déclarer dans le message et présent dans le bucket S3
     * - lancer le traitement batch
     * - uploader le fichier générer dans le bucket S3
     * - envoyer un message dans la file de message
     *
     * @param message
     * @return true si ok false sinon
     * @throws Exception
     */
    Boolean processAndNotify(Message message) throws Exception;

}
