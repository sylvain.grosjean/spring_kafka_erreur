package fr.pe.client.stream.kafka.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import fr.pe.client.stream.kafka.domain.Message;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.task.configuration.EnableTask;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
@Slf4j
@EnableTask
@EnableBatchProcessing
public class ProcessMessageServiceImpl implements ProcessMessageService {

    Logger log = LoggerFactory.getLogger(Logger.class);

    private S3ObjectService s3ObjectService;
    private ResourceLoader resourceLoader;

    @Value("${amazon.s3.bucket-name}")
    public String bucketName;

    @Value("${amazon.s3.end-point-url}")
    public String endpointUrl;

    @Value("${app.directory.reception}")
    public String dirReception;

    public ProcessMessageServiceImpl(S3ObjectService s3ObjectService
                                     ) {
        this.s3ObjectService = s3ObjectService;
        this.resourceLoader = resourceLoader;

    }

    @Override
    public Boolean processAndNotify(Message messageIn) throws Exception {

        String urlObject = messageIn.getUrlObjectBucket();
        log.info("urlObject: " +urlObject);
        // extraire la clef correspondant au fichier (info 2 : correspond au 2 '/' present dans l'url
        // endpointurl/bucketname/keyfile
        Integer posExtractUrl = endpointUrl.length() + bucketName.length() + 2;
        log.info("posExtractUrl: " +posExtractUrl);
        String keySource = urlObject.substring(posExtractUrl);
        log.info("keySource: " +keySource);
        String fileNormea =  messageIn.getIdClient() + "-" + keySource;
        log.info("fileNormea: " +fileNormea);
        String fileJson = messageIn.getIdClient() + "-identite-certifie.json";

        Path path = Paths.get(dirReception + fileNormea);
        //télécharger le fichier normea présent dans le bucket dans les resources de l'apli"
        if (s3ObjectService.downloadFile(keySource, path)) {
            log.info("Successfully download");
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }

    }

}